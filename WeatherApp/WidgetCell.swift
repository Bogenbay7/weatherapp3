//
//  WidgetCell.swift
//  WeatherApp
//
//  Created by Nurba on 20.04.2021.
//

import UIKit

class WidgetCell: UITableViewCell {
    
    var delegate: UIViewController?
    
    static let identifier = String(describing: WidgetCell.self)
    static let nib = UINib(nibName: identifier, bundle: nil)
    

    
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var windSpeed: UILabel!
    @IBOutlet weak var feelsLike: UILabel!
    @IBOutlet weak var pressure: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
